#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

struct serverRequest {
	long mtype;
	enum { get, new, edit } req; //for get: empty, new: data, edit: id/adjustment
	int id;
    int returnPid;
	char data[100];
	int adjustment;
};

struct yak{
    char data[100];
    int id;
    int score;
};

struct yakList {
    long mtype;
    int count;
    struct yak yaks[10];
};



void printBorder();
void refresh(int msqid);
void vote(int id, int value, int msqid); //value is the +1 or -1
void postNew(char string[100], int msqid);

struct yakList mYakList;

int main(){
	int msqid;
    key_t key;
	char command;
	char makID;
    int id;

	if((key = ftok("MikMak.c", 'B')) == -1){
		perror("ftok");
		exit(1);
	}

	if ((msqid = msgget(key, 0665)) == -1){
		perror("msgget");
		exit(1);
	}

    refresh(msqid);
    
	while(1){
        printf("[R]eply U[p] D[own] S[end] E[xit]: ");
		scanf(" %c", &command);
        if(command == 'R'){
            refresh(msqid);
        } else if (command == 'D' || command == 'U'){
            scanf(" %d", &id);
            if(command == 'D'){
                vote(id,-1,msqid);
            } else {
                vote(id,1,msqid);
            }
        } else if (command == 'S'){
            char string[100];
            gets(string);
            postNew(string,msqid);
        } else if (command == 'E'){
            exit(1);
        } else {
            printf("\n Proper usage: <Letter> <Data>");
        }
		
	}
}

void postNew(char string[100], int msqid){
    struct serverRequest buf;
    buf.mtype = 1;
    buf.req = new;
    strcpy(buf.data,string);
    if( msgsnd(msqid, &buf, sizeof(struct serverRequest) - sizeof(long), 0) == -1){
        perror("msgsnd");
    }

}

void vote(int id, int value, int msqid){
    struct serverRequest buf;
    buf.mtype = 1;
    buf.req = edit;
    buf.adjustment = value;
    buf.id = mYakList.yaks[id-1].id;
    if( msgsnd(msqid, &buf, sizeof(struct serverRequest) - sizeof(long), 0) == -1){
        perror("msgsnd");
    }
}


void refresh(int msqid){
    struct serverRequest buf;
    buf.mtype = 1;
    buf.returnPid = getpid();
    printBorder();
    buf.req = get;
    if( msgsnd(msqid, &buf, sizeof(struct serverRequest) - sizeof(long), 0) == -1){
        perror("msgsnd");
    }
    
    if( msgrcv(msqid, &mYakList, sizeof(struct yakList) - sizeof(long),getpid(),0) == -1){
        perror("mgsrcv");
        exit(1);
    }
    
    int i;
    for(i = 0; i < mYakList.count; i ++){
        if(mYakList.yaks[i].score > 0){
            printf("%d. +%d %s \n", i+1,mYakList.yaks[i].score, mYakList.yaks[i].data);
        } else if (mYakList.yaks[i].score < 0){
            printf("%d. %d %s \n", i+1,mYakList.yaks[i].score, mYakList.yaks[i].data);
        } else {
            printf("%d.  %d %s \n", i+1,mYakList.yaks[i].score, mYakList.yaks[i].data);
        }
    }
    printf("\n");
}

void printBorder(){
	int i;
	for(i = 0; i < 74; i++){
		printf("_");
	}
	printf("\n");
}