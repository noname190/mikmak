#include <stdlib.h>
#include <sys/msg.h>
#include <stdio.h>
#include <string.h>

struct serverRequest {
	long mtype;
	enum { get, new, edit } req; //for get: empty, new: data, edit: id/adjustment
	int id;
    int returnPid;
	char data[100];
	int adjustment;
};

struct yak{
    char data[100];
    int id;
    int score;
};

struct yakList {
    long mtype;
    int count;
    struct yak yaks[10];
};

void sort();
void adjustment(int value, int id);
void addNew(char string[100]);

struct yakList yaklist;

int lastId;

int main(){

    
	int msqid;
	key_t key;
   
    
    
    
    //debug start
    strcpy(yaklist.yaks[0].data, "I feel like icecream");
    yaklist.yaks[0].score = 5;
    yaklist.yaks[0].id = 0;
    
    strcpy(yaklist.yaks[1].data, "Who are you sitting at DM starbucks in the red top?");
    yaklist.yaks[1].score = 7;
    yaklist.yaks[1].id = 1;
    
    strcpy(yaklist.yaks[2].data, "Anyone going to party in Cancun next month?");
    yaklist.yaks[2].score = 0;
    yaklist.yaks[2].id = 2;
    
    strcpy(yaklist.yaks[3].data, "I thought Yik Yak at 6ix would be poppin. Guess its only dope at uni");
    yaklist.yaks[3].score = -3;
    yaklist.yaks[3].id = 3;

    
    yaklist.count = 4;
    
    lastId = 3;
    //debug end
    
    
	if((key = ftok("MikMak.c", 'B')) == -1){
		perror("ftok");
		exit(1);
	}


	if ((msqid = msgget(key, 0665 | IPC_CREAT)) == -1) {//create the queue
        perror("msgget");
        exit(1);
    }
    
    
 	 while(1) {
 	 	struct serverRequest buf;
         
    	if( msgrcv(msqid, &buf, sizeof(struct serverRequest) - sizeof(long) ,1,0) == -1){
           perror("mgsrcv");
           exit(1);
        }
         
        if(buf.req == get){
            sort();
            yaklist.mtype = buf.returnPid;
            if( msgsnd(msqid, &yaklist, sizeof(struct yakList) - sizeof(long), 0) == -1){
                perror("msgsnd");
            }
        } else if(buf.req == edit){
            adjustment(buf.adjustment, buf.id);
        }else if (buf.req == new){
            addNew(buf.data);
        }

        
        fflush(stdout);
    }
}

void addNew(char string[100]){
    lastId++;
    yaklist.count++;
    yaklist.yaks[lastId].id = lastId;
    yaklist.yaks[lastId].score = 0;
    strcpy(yaklist.yaks[lastId].data,string);
}

void sort(){
    struct yak y;
    int i,j;
    int n = yaklist.count;
    for (i = 0; i < n; ++i)
    {
        for (j = i + 1; j < n; ++j)
        {
            if (yaklist.yaks[i].score < yaklist.yaks[j].score)
            {
                y =  yaklist.yaks[i];
                yaklist.yaks[i] = yaklist.yaks[j];
                yaklist.yaks[j] = y;
            }
        }
    }

}

void adjustment(int value, int id){
    printf("adjusting");
    int i;
    for(i = 0; i < yaklist.count; i++){
        if(id == yaklist.yaks[i].id){
            yaklist.yaks[i].score = yaklist.yaks[i].score + value;
            if( yaklist.yaks[i].score <= -5){
                int loc = i;
                printf("%d", loc);
                while (loc < yaklist.count) {
                    yaklist.yaks[loc] = yaklist.yaks[loc+1];
                    loc++;
                }
                yaklist.count--;
            }
            break;
        }
    }
    
}